function [A] = get_lfw_15(id_lfw)
%   Get a collection of people having equal or more than 15 images
A = [0 0 0];
image = 1;
num_images=0;
for i = 1: length(id_lfw)
    if id_lfw(i) == image
        num_images = num_images + 1;
    end
    if id_lfw(i) ~= image
        if num_images >= 15
            %update
            A = [A; i image num_images];
        else
        end
        image = id_lfw(i);
        num_images = 0;
    end
end
A(1, :) = [];
end

