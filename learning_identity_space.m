function [a] = learning_identity_space(person_id, start, offset , lbp_lfw, save_dir)


dataType = ['Individual-' num2str(person_id)]
dataSetNames = 'LFW'
c=3

if ~exist('itNo')           ,itNo = [500, 1500, 2000]; end
if ~exist('indPoints')    ,  indPoints = 120; end
if ~exist('initVardistIters'), initVardistIters = 180; end
if ~exist('mappingKern')   ,  mappingKern = {'rbfard2', 'white'}; end
if ~exist('vardistCovarsMult'),  vardistCovarsMult=1.3; end
if ~exist('dataSetNames')    ,    dataSetNames = {};    end
if ~exist('invWidthMult'),       invWidthMult = 5; end
if ~exist('dataType'), dataType = 'default'; end
if ~exist('latentDimPerModel')    ,  latentDimPerModel = 5;          end
if ~exist('latentDim'), latentDim = 100; end
if ~exist('initLatent'),     initLatent ='ppca';   end
if ~exist('initX'),     initX ='ppca';   end 
if ~exist('numSharedDims'), numSharedDims = 2; end
if ~exist('experimentNo'), experimentNo = 1; end
if ~exist('doPredictions'), doPredictions = false; end
if ~exist('DgtN'), DgtN = false; end
if ~exist('indTr'), indTr = -1; end

enableParallelism = 0;
%% Getting data

X = double(lbp_lfw(start: start + offset ,:));

c = 3;
Yall{1} = X(1:5, :);
Yall{2} = X(6:10, :);
Yall{3} = X(11:15, :);

svargplvm_init;

numberOfDatasets = length(Yall);

for i=1:numberOfDatasets
    Y = Yall{i};
    dims{i} = size(Y,2);
    N{i} = size(Y,1);
    if indTr == -1
        indTr = 1:N{i};
    end
    indTs = setdiff(1:size(Y,1), indTr);
    Ytr{i} = Y(indTr,:); 
    Yts{i} = Y(indTs,:);
    t{i} = linspace(0, 2*pi, size(Y, 1)+1)'; t{i} = t{i}(1:end-1, 1);
    timeStampsTraining{i} = t{i}(indTr,1); 
    d{i} = size(Ytr{i}, 2);
end

for i=2:numberOfDatasets
    if N{i} ~= N{i-1}
        error('The number of observations in each dataset must be the same!');
    end
end

%--Options for the models
for i=1:numberOfDatasets
    options{i} = vargplvmOptions('dtcvar');
    options{i}.kern = mappingKern;
    options{i}.numActive = indPoints;
    options{i}.optimiser = 'scg2';
    if ~DgtN
        options{i}.enableDgtN = false;
    end
    options{i}.scaleVal = sqrt(var(Ytr{i}(:)));
end

for i=1:length(Ytr)
    % Compute m, the normalised version of Ytr (to be used for
    % initialisation of X)
    bias = mean(Ytr{i});
    scale = ones(1, d{i});
    
    if(isfield(options{i},'scale2var1'))
        if(options{i}.scale2var1)
            scale = std(Ytr{i});
            scale(find(scale==0)) = 1;
            if(isfield(options{i}, 'scaleVal'))
                warning('Both scale2var1 and scaleVal set for GP');
            end
        end
    end
    if(isfield(options{i}, 'scaleVal'))
        scale = repmat(options{i}.scaleVal, 1, d{i});
    end
    
    m{i} = Ytr{i};
    for j = 1:d{i}
        m{i}(:, j) = m{i}(:, j) - bias(j);
        if scale(j)
            m{i}(:, j) = m{i}(:, j)/scale(j);
        end
    end
    
end

%-- Init latent space%
for i=1:length(Ytr)
    bias = mean(Ytr{i});
    scale = ones(1, d{i});
    
    if(isfield(options{i},'scale2var1'))
        if(options{i}.scale2var1)
            scale = std(Ytr{i});
            scale(find(scale==0)) = 1;
            if(isfield(options{i}, 'scaleVal'))
                warning('Both scale2var1 and scaleVal set for GP');
            end
        end
    end
    if(isfield(options{i}, 'scaleVal'))
        scale = repmat(options{i}.scaleVal, 1, d{i});
    end
    
    m{i} = Ytr{i};
    for j = 1:d{i}
        m{i}(:, j) = m{i}(:, j) - bias(j);
        if scale(j)
            m{i}(:, j) = m{i}(:, j)/scale(j);
        end
    end
    
end

X_init = svargplvmInitLatentSpace(Ytr, d, options, initLatent, latentDim, latentDimPerModel, numSharedDims);


latentDim = size(X_init,2);

clear('Y')

%Create the sub-models
for i=1:numberOfDatasets
    fprintf(1,'# Creating the model...\n');
    options{i}.initX = X_init;
    model{i} = vargplvmCreate(latentDim, d{i}, Ytr{i}, options{i});

    model{i}.X = X_init; %%%%%%%
    model{i} = vargplvmParamInit(model{i}, m{i}, model{i}.X);
    model{i}.X = X_init; %%%%%%%
    
    inpScales = invWidthMult./(((max(model{i}.X)-min(model{i}.X))).^2); % Default 5
    %inpScales(:) = max(inpScales); % Optional!!!!!
    model{i}.kern.comp{1}.inputScales = inpScales;
    
    if strcmp(model{i}.kern.type, 'rbfardjit')
        model{i}.kern.inputScales = model{i}.kern.comp{1}.inputScales;
    end
    params = vargplvmExtractParam(model{i});
    model{i} = vargplvmExpandParam(model{i}, params);
    model{i}.vardist.covars = 0.5*ones(size(model{i}.vardist.covars)) + 0.001*randn(size(model{i}.vardist.covars));
    
    
    
    model{i}.beta=1/(0.01*var(m{i}(:)));
    prunedModelInit{i} = vargplvmPruneModel(model{i}); 
end

%--- Model
svargplvm_init
model = svargplvmModelCreate(model);
model.dataSetNames = dataSetNames;
model.experimentNo = experimentNo;
model.dataType = dataType;

%%---
capName = dataType;
capName(1) = upper(capName(1));
modelType = model.type;
modelType(1) = upper(modelType(1));
fileToSave = [save_dir 'bayesian-train-' capName modelType num2str(experimentNo) '.mat'];
%%---


params = svargplvmExtractParam(model);
model = svargplvmExpandParam(model, params);

display = 1;

if initVardistIters ~=0
    model.initVardist = 1; model.learnSigmaf = 0;
    model = svargplvmPropagateField(model,'initVardist', model.initVardist);
    model = svargplvmPropagateField(model,'learnSigmaf', model.learnSigmaf);
    fprintf(1,'# Intitiliazing the variational distribution...\n');
    model = svargplvmOptimise(model, display, initVardistIters); % Default: 20
    %fprintf(1,'1/b = %.4d\n',1/model.beta);
    
    modelInitVardist = model;
    model.initVardistIters=initVardistIters;
end

model.initVardist = 0; model.learnSigmaf=1;
model = svargplvmPropagateField(model,'initVardist', model.initVardist);
model = svargplvmPropagateField(model,'learnSigmaf', model.learnSigmaf);

model.iters = 0;
for i=1:length(itNo)
    iters = itNo(i); % default: 2000
    fprintf(1,'\n# Optimising the model for %d iterations (session %d)...\n',iters,i);
    model = svargplvmOptimise(model, display, iters);
    model.iters = model.iters + iters;
    % Save model
    prunedModel = svargplvmPruneModel(model);
    fprintf(1,'# Saving %s\n',fileToSave);
    save(fileToSave, 'prunedModel', 'prunedModelInit');
end

thresh = 0.0005;

s1 = model.comp{1}.kern.comp{1}.inputScales;
s2 = model.comp{2}.kern.comp{1}.inputScales;
s3 = model.comp{3}.kern.comp{1}.inputScales;

s1 = s1 / max(s1);
s2 = s2 / max(s2);
s3 = s3 / max(s3);

retainedScales{1} =find(s1 > thresh);
retainedScales{2} =find(s2 > thresh);
retainedScales{3} =find(s3 > thresh);

shareDims = intersect(retainedScales{1}, retainedScales{2});
shareDims = intersect(retainedScales{3}, shareDims);

shareDimsFile = [save_dir 'shareDims-' num2str(person_id) '.mat'];
save(shareDimsFile, 'shareDims');
a = 0
end