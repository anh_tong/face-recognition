function [W,iW,nlatlen,sf,sn,Vx,nlatvar] = get_hyp(cov,hyp,d,mode)
    if numel(cov)==0, cov = {'covSum', {'covSEard','covNoise'}}; end  
    if strcmp(cov{1},'covSum') && strcmp(cov{2}(2),'covNoise')
        if strcmp(cov{2}(1),'covSEard'),     nlatlen = d;
        elseif strcmp(cov{2}(1),'covSEiso'), nlatlen = 1;           
        else error('type of covariance function not supported'), end
        if numel(hyp)<nlatlen+2, error('not enough hyperparameters'), end    
        iW = diag(exp(-2*hyp(1:nlatlen)))*eye(d); 
        sf = exp(hyp(nlatlen+1)); sn = exp(hyp(nlatlen+2)); 
        nlatvar = length(hyp)-nlatlen-2;  
        if nlatvar==0
            if numel(strfind(mode,'gauss'))             
                Vx = eye(d);               
            else
                Vx = 0;                                   
            end
        elseif nlatvar==1 || nlatvar==d
            if numel(strfind(mode,'gauss'))             
                dgVx = exp(2*hyp( nlatlen+2+(1:nlatvar) ));
                Vx = diag(dgVx)*eye(d);  
            else
                error('wrong number of hyperparameters')
            end
        else
            error('wrong number of hyperparameters')
        end
    else
        error('type of covariance function not supported')
    end
    W = inv(iW);

