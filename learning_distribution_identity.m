clear all;

load id_lfw;
load lbp_lfw;
learned = './learned/';

people = get_lfw_15(id_lfw);
min_share_dims = 10000; % a big number
Z = [];
% find the mininum dimension of latent space
for i = 1: size(people,1)
    person_id = people(i,1);
    shareDimsFile = [learned 'shareDims-' num2str(person_id) '.mat'];
    load(shareDimsFile);
    if size(shareDims,2) < min_share_dims
        min_share_dims = size(shareDims,2);
    end
end

for i = 1: size(people,1)
    person_id = people(i,1);
    shareDimsFile = [learned 'shareDims-' num2str(person_id) '.mat'];
    load(shareDimsFile);
    modelFile = [learned 'bayesian-train-Individual-' num2str(person_id) 'Svargplvm1.mat'];
    load(modelFile);
    if size(shareDims, 2) == min_share_dims
        Z{i} = prunedModel.X(:, shareDims);
    else
        print('Choose the min_share_dims components having the largest weights')
    end
end


%% generate K matched pairs 
k = 1;
for i = 1: size(people,1)
    person_id = people(i,1);
    start = people(i,2);
    offset = people(i,3);
    z_part = Z{i};
    x_part = double(lbp_lfw(start: start + 15 ,:));
    perm = randperm(15,15);
    for j = 1:15
        if j ~= perm(j)
            K_same{k}.Z = [z_part(1,:) z_part(1,:)];
            K_same{k}.X = [x_part(j,:) x_part(perm(j),:)];
            k = k+1;
        end
    end
    
    if i == 3
        break;
    end
end

K = k;
k = 1;

%% generate K mismatched pairs
num_of_people = size(people,1);
while k < K + 1
    perm = randperm(num_of_people, num_of_people);
    for i = 1:num_of_people
        person_id = people(i,1);
        start = people(i,2);
        offset = people(i,3);
        z_part = Z{i};
        x_part = double(lbp_lfw(start: start + 15 ,:));
        j = perm(i);
        if i ~= j
            m_person_id = people(j,1);
            m_start = people(j,2);
            m_offset = people(j,3);
            m_z_part = Z{j};
            m_x_part = double(lbp_lfw(m_start: m_start + 15 ,:));
            rng = randi(15);
            K_diff{k}.Z = [z_part(1,:) m_z_part(1,:)];
            K_diff{k}.X = [x_part(rng,:) m_x_part(rng,:)];
            if k == K 
                break
            end;
            k = k + 1;
        end
    end
    if k == K 
       break 
    end;
end

z = [];
x = [];
for i = 1:length(K_same)
    z = [z; K_same{i}.X];
    x = [x; K_same{i}.Z];
end
x = x';
z = z';

L = 2;
Sigma = 'full';

GMModel = fitgmdist(x', L, 'CovarianceType',Sigma, 'SharedCovariance', true, 'Regularize', 1e-5);
lambda = GMModel.ComponentProportion;
clusterX = cluster(GMModel,x');

cov = {'covSum', {'covSEiso','covNoise'}}; 
len = 0.25; 
sf  =  0.25;  
sn  =  0.02;  
mode = 'l2o mni gauss';
hyp0 = log([len; sf; sn]);
x0 = my_gplvmSE_2(hyp0, cov, zeros(size(x,2),0), z); 
x = x0; hyp = hyp0; 
md = ['dhyp ',mode];

[txt,hyp,fhyp] = evalc('minimize(hyp,''my_gplvmSE_2'',-15,cov,x,z,md)');

mu = my_gplvmSE_2(hyp, cov, zeros(size(x,2),0), z);
[W,iW,nlatlen,sf,sn,Vx,nlatvar] = get_hyp(cov,hyp,d,mode);
fileToSave = [learned 'distribution.mat']
save(fileToSave, 'mu', 'W');
