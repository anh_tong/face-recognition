function [out1, out2, out3, W,Vx,sn,sf] = my_gplvmSE_2(hyp, cov, X, Z, mode, XZstar)
% Gaussian process latent variable model with the following possible covariance
% functions: 
%     (i)  cov = {'covSum', {'covSEiso','covNoise'}};
%     (ii) cov = {'covSum', {'covSEard','covNoise'}}; [default]
%          hyp = log([len; sf; sn <;sqrt(dgVx)>]);
%
% INPUT:
% hyp    (d+2)x1 vector (covSEard) or (1+2)x1 vector (covSEiso) containing 
%        lengthscale parameters, signal and noise variance
%        if mode contains 'gauss', the vector can be used to set the diagonal
%        elements of Vx: the sizes (d+2+d)x1 (d+2+1)x1 are valid for covSEard
%        and the sizes (1+2+d)x1 (1+2+1)x1 are valid for covSEard
%        default for Vx = eye(d) in case hyp is too short
% X      dxn matrix of inputs,  we have n inputs  from IR^d
% Z      DxN matrix of targets, we have N targets from IR^D
% mode   {'','loo','lpo gauss'}, {'dX','dhyp','dXhyp'}, 'lbc', 'mni'
%        loo means leave-one-out, l<p>o means leave-p-out i.e. specify l1o. l2o,
%        l3o, l4o if more points are to be left out (loo is equivalent to l1o)
% 
% OUTPUT:
% nlZ    1x1 negative log marginal likelihood
%
% linear backconstraints X = P*Z can be used by having 'lbc' in mode,
%   optimization is then done over P rather over X (no mean subtraction)
%   this is possible for all three gradient modes 1b), 2b) and 2c)
%
% usage of the different modes
% 0) explicit PCA initialisation if norm(X,'fro')==0
%   X = gplvmSE(hyp, cov, zeros(d,0), Z);
%
% 1a) negative log marginal likelihood evaluation
%   nlZ = gplvmSE(hyp, cov, X, Z);
% 1b) derivatives w.r.t. negative log marginal likelihood
%   [nlZ, dnlZ_dhyp, X] = gplvmSE(hyp, cov, X, Z, 'dhyp');
%   [nlZ, dnlZ_dX,   X] = gplvmSE(hyp, cov, X, Z, 'dX'  );
%   [nlZ, dnlZ_dX,   X] = gplvmSE(hyp, cov, X, Z, 'dXhyp'  );
%
% 2a) negative log LOO surrogate likelihood evaluation
%   nlo = gplvmSE(hyp, cov, X, Z, 'loo');        latent delta distributions
%   nlo = gplvmSE(hyp, cov, X, Z, 'loo gauss');  latent normal distributions
% 2b) derivatives w.r.t. negative log LOO surrogate likelihood
%   [nlo, dnlo_dhyp, X] = gplvmSE(hyp, cov, X, Z, 'loo dhyp');
%   [nlo, dnlo_dX,   X] = gplvmSE(hyp, cov, X, Z, 'loo dX'  );
%   [nlo, dnlo_dX,   X] = gplvmSE(hyp, cov, X, Z, 'loo dXhyp');
%   [nlo, dnlo_dhyp, X] = gplvmSE(hyp, cov, X, Z, 'loo gauss dhyp' );
%   [nlo, dnlo_dX,   X] = gplvmSE(hyp, cov, X, Z, 'loo gauss dX'   );
%   [nlo, dnlo_dhyp, X] = gplvmSE(hyp, cov, X, Z, 'loo gauss dXhyp');
%   including an 'mni' in cases 2a) and 2b) results in a different computation
%   of the nlo criterion; the mean prediction for latent point xi is computed
%   neglecting the contribution of xi
% 2c) additionally, one can include a sixth input argument Zstar to enrich the
%   derivative computations to take into account also "validation" points
%   different from the actual test points; they are used to prevent overfitting
%   [nlo, dnlo_dhyp, X] = gplvmSE(hyp, cov, X, Z, 'loo dhyp',  Zstar);
%   [nlo, dnlo_dX,   X] = gplvmSE(hyp, cov, X, Z, 'loo dX',    Zstar );
%   [nlo, dnlo_dhyp, X] = gplvmSE(hyp, cov, X, Z, 'loo dXhyp', Zstar);
%   [nlo, dnlo_dhyp, X] = gplvmSE(hyp, cov, X, Z, 'loo gauss dhyp',  Zstar );
%   [nlo, dnlo_dX,   X] = gplvmSE(hyp, cov, X, Z, 'loo gauss dX' ,   Zstar );
%   [nlo, dnlo_dhyp, X] = gplvmSE(hyp, cov, X, Z, 'loo gauss dXhyp', Zstar );
%
% 3a) density evaluation in Z space (log density not negative log density)
%   [ld,Mz,Vz] = gplvmSE(hyp, cov, X, Z, '', Zstar);
%   [ld,Mz,Vz] = gplvmSE(hyp, cov, X, Z, 'gauss', Zstar);
%
% 3b) forward projection of latent points
%   [Mzstar,Vzstar] = gplvmSE(hyp, cov, X, Z, '', Xstar);
%   [Mzstar,Vzstar] = gplvmSE(hyp, cov, X, Z, 'gauss', Xstar);
%
% Hannes Nickisch, created 01 April 2009, last modified 25 September 2009
%
% TODO: - implementation of computationally attractive approximations to the
%         covariance matrix
%       - include the switches dgfix, scfix and valonly (excludes mni) in the 
%         above documentation

%


show_debug = 0;    % 0 or 1 to control the amount of info plotted on the console

if nargin<5, mode=''; end                          % no mode given, default mode
if numel(strfind(mode,'lbc'))     % check whether we use linear back-constraints
    lbc = 1;
    if show_debug, fprintf('Using linear backconstraints.\n'), end
else
    lbc = 0;
end
[D,N] = size(Z); [d,n] = size(X);             % data dimension, latent dimension
muz = sum(Z,2)/N;                                            % compute mean of Z
if n==0, n = N; end
if norm(X,'fro') == 0                  % PCA init of X norm of X is exactly zero
    if show_debug, fprintf('PCA init\n'), end
    Cz = Z-muz*ones(1,N); Cz = (Cz*Cz')/(N-1);   % covariance
    [V,L] = eig(Cz);                             % eigen-decomposition
    [l,id] = sort(diag(L),'descend'); V = V(:,id);
    P = diag( 1./sqrt(l(1:d)) )*V(:,1:d)';
    if lbc, X = P; else X = P*(Z-muz*ones(1,N)); end
    if nargout==1, out1 = X; return, end
end, clear n

if lbc, P = X; X = X*(Z-muz*ones(1,N)); end  % compute X for backconstraint case

% check for covariance function and correct number of hyperparameters
[W,iW,nlatlen,sf,sn,Vx,nlatvar] = get_hyp(cov,hyp,d,mode);
if show_debug, fprintf('%s ',cov{2}{1}), end
clear cov hyp % not needed beyond this point: use W,iW,sf,sn,Vx,nlatlen,nlatvar

% compute covariance matrix, K = feval(cov{:}, hyp, X'); 
R = chol(iW); K = sn^2*eye(N) + sf^2*exp(-sq_dist(R*X)/2);
L = chol(K); 
logdetK = 2*sum( log(diag(L)) ); % log(det(K))
Alpha = solve_chol(L,Z'); % iK*Z'
iK  = L\(L'\eye(N)); % inv(K)

% Are we in leaave-p-out mode? determine p, default is 1
lo_str = regexp(mode, 'l([0-9]+|o)o', 'match'); lo_mod = numel(lo_str)>0;
p=1; if lo_mod, p=max(sscanf(lo_str{1},'l%do'),1); end, if ~numel(p), p=1; end

% switch between the three major modes: 
%   (1) marginal likelihood, (2) leave-out  and  (3) density
grad_mode    = numel(strfind(mode,'dX')) || numel(strfind(mode,'dhyp'));
lZ_mode      = nargin==4 || ( nargin==5 && ~lo_mod );
lo_mode      = nargin==5 || ( nargin==6 && grad_mode );
den_mode     = nargin==6 && ~grad_mode;
mni_mode     = numel(strfind(mode,'mni'));      % leave-one-out from the mean
dgfix_mode   = numel(strfind(mode,'dgfix'));    % fix covariance's diagonal
scfix_mode   = numel(strfind(mode,'scfix'));    % fix covariance's scale
valonly_mode = numel(strfind(mode,'valonly'));  % validation set only for grads

addVz = 0; mulVz = 1; % default values
% fix diagonal if desired; only in nlo gauss mode
if dgfix_mode && numel(strfind(mode,'gauss'))
    if show_debug, fprintf('diagonal fix mode\n'), end
    zm = sum(Z,2)/N; Z_zm = Z-repmat(zm,[1,N]);
    Cz = (Z_zm*Z_zm')/(N-1); dgCz = diag(Cz);
    % check whether the data is prewhitened
    if norm(Cz-diag(dgCz),'fro')>1e-9 || sum(dgCz>1.1)
        warning('data has not been prewhitened');
    else
        addVz = diag(max(dgCz)-dgCz);
    end               
end
% fix scale if desired; only in nlo gauss mode
if scfix_mode && numel(strfind(mode,'gauss'))
    if show_debug, fprintf('scale fix mode\n'), end
    zm = sum(Z,2)/N; Z_zm = Z-repmat(zm,[1,N]);
    Cz = (Z_zm*Z_zm')/(N-1); dgCz = diag(Cz);
    % check whether the data is prewhitened
    if norm(Cz-diag(dgCz),'fro')>1e-9 || sum(dgCz>1.1)
        warning('data has not been prewhitened');
    else
        mulVz = sqrt(dgCz)*sqrt(dgCz)';
    end               
end

%% marginal likelihood mode
if lZ_mode
    if show_debug, fprintf('nlZ mode\n'), end
    
    % calculate negative log marginal likelihood
    nlZ = ( D*N*log(2*pi) + D*logdetK + sum(sum(iK.*(Z'*Z))) )/2;
    out1 =  nlZ;
    
    % calculate derivatives
    if nargout>1
        % intermediate quantity: derivative w.r.t. K
        % ZiK  = Z*iK;  dnlZ_dK = (D/2)*iK -(ZiK'*ZiK)/2;
        dnlZ_dK = (D/2)*iK -(Alpha*Alpha')/2;
        % apply chain rule to get derivatives w.r.t. X and hyp
        out2 = chainK_til(iW,nlatlen,sn,X,K,dnlZ_dK,mode);
        % apply linear back constraints
        if lbc && numel(strfind(mode,'dX')), out2 = out2*Z'; end
        if lbc, out3 = P; else out3 = X; end
        % append dhyp if both are desired
        if numel(strfind(mode,'dXhyp'))
            out2 = {out2, chainK_til(iW,nlatlen,sn,X,K,dnlZ_dK,'dhyp')};
        end
    end
    if show_debug, fprintf('nlZ=%1.3f.\n',out1), end
    
%% leave-out mode
elseif lo_mode
    % Gaussian Mixture model on latent identity space
    L = 4;
    Sigma = 'full';
    GMModel = fitgmdist(X', L, 'CovarianceType',Sigma, 'SharedCovariance', true, 'Regularize', 1e-5);
    lambda = GMModel.ComponentProportion;
    clusterX = cluster(GMModel,X');
    %
    if show_debug, fprintf('L%d0 mode\n',p), end
    if numel(strfind(mode,'gauss'))
        if show_debug, fprintf('stochastic input\n'), end
    else
        if show_debug, fprintf('deterministic input\n'), end
    end
    if nargin==6
        if show_debug, fprintf('validation points used\n'), end
        Nstar = size(XZstar,2);
    else
        Nstar = 0; XZstar = [];
    end
    if mni_mode && show_debug, fprintf('latent point xi removed from mean\n'), end
    
    % compute full mixture: for stochastic or deterministic input
    [Mz,Vz,iVz,logdetVz,Ks_til] = projGauss(K,L,iK,Z,Alpha,sn,sf,iW,Vx,X,[],mulVz,addVz);  

    if mni_mode % compute all leave-xi-out mean predictions, evaluate mixture
        Mzni = zeros(D,N,N);
        for i=1:N
            ni = [1:i-1,i+1:N]; % all other indices
            % Mzni from Mz via rank one updates; complexity O(ND)
            ki = K(ni,i);  vi = iK(ni,ni)*ki;  ai = ki'*vi;  wi = Z(:,ni)*vi;
            Mzni(:,ni,i) = Mz(:,ni) - Alpha(i,:)'*Ks_til(i,ni) ...    a bunch of
                         - Z(:,i)*(iK(i,ni)*Ks_til(ni,ni)) ...  rank one updates
                         - wi*( (vi'*Ks_til(ni,ni))/(K(i,i)+ai));
            % equivalent to but much faster than: 
            % Mzni(:,ni,i) = Z(:,ni)*( K(ni,ni)\Ks_til(ni,ni) );
        end
        log_Gauni = evalMixture(Mzni,iVz,logdetVz,Z);   % eval. mixture      
        lsomask = get_lsomask(log_Gauni, p, clusterX)
        log_Gauni(lsomask) = -Inf;     % leave out according to validation scheme
        [lsGauni,Gauni] = logsum2exp(log_Gauni);
        out1 = -sum(lsGauni);
    else % evaluate full mixture
        log_Gau = evalMixture(Mz,iVz,logdetVz,[Z,XZstar]);       % eval. mixture
        lsomask = get_lsomask(log_Gauni, p, clusterX)
%         lomask = get_lomask(log_Gau,p);
        if size(lomask,1)<N+Nstar, lomask(N+Nstar,1) = false; end  % extend mask
        log_Gau(lomask) = -Inf;       % leave out according to validation scheme
        if valonly_mode % only look at validation points for the function
            if show_debug, fprintf('eval. mixture only on Zstar\n'), end
            log_Gau(1:N,:) = -Inf; % keep Zstar only
        end
        [lsGau,Gau] = logsum2exp(log_Gau);         % numerically save version of
        out1 = -sum(lsGau);           % nlo = -sum(log(sum( exp(log_Gau), 2 )));
        if valonly_mode, out1 = -sum(lsGau(N+1:end)); end
    end
    % NOTE: Both Gauni and Gau are equal to exp(log_Gauni) and exp(log_Gau) only
    % up to a constant factor. This scales the objective AND the derivative in
    % the same way so that all gradient computations are OK.
    
    % compute the derivatives
    % note that the objective is -lo and not lo
    % dlo_dVz is either of size 1x1xN (det. input) or DxDxn (stoch. input)
    % dlo_dMz is of size DxN
    if nargout>1
        dlo_dVz = zeros(size(iVz));       
        if mni_mode       % compute all leave-xi-out mean derivatives in O(D²N²)
            dlo_dMzni = zeros(size(Mzni));
            sGauni = sum(Gauni,2);
            for i=1:N
                zi = Z(:,i);
                for j=[1:i-1,i+1:N] % all other indices
                    vij = iVz(:,:,j)*(zi-Mzni(:,j,i));
                    % switch between deterministic (isotropic) and stoch. case
                    if size(iVz,1)==D
                        dGau_dVz = (vij*vij' - iVz(:,:,j)).*mulVz; % stoch. case
                    else
                        dGau_dVz = vij'*vij - D*iVz(:,:,j);     % isotropic case
                    end
                    dlo_dVz(:,:,j) = dlo_dVz(:,:,j)+Gauni(i,j)*dGau_dVz/(2*sGauni(i));
                    dlo_dMzni(:,j,i) = Gauni(i,j)*vij/sGauni(i);
                end
            end
        else                           % compute all mean derivatives in O(D²N²)
            dlo_dMz = zeros(size(Mz));
            sGau = sum(Gau,2);
            if valonly_mode, N0 = N+1; else N0 = 1; end
            for i=N0:N+Nstar    % det: O(N²+Nstar*N), stoch: O(N²*D²+Nstar*N*D²)
                if i<=N, zi = Z(:,i); else zi = XZstar(:,i-N); end
                for j=[1:min(i-1,N),i+1:N]
                    vij = iVz(:,:,j)*(zi-Mz(:,j));
                    % switch between deterministic (isotropic) and stoch. case
                    if size(iVz,1)==D
                        dGau_dVz = (vij*vij' - iVz(:,:,j)).*mulVz; % stoch. case
                    else
                        dGau_dVz = vij'*vij - D*iVz(:,:,j);     % isotropic case
                    end
                    dlo_dVz(:,:,j) = dlo_dVz(:,:,j)+Gau(i,j)*dGau_dVz/(2*sGau(i));
                    dlo_dMz(:,j)   = dlo_dMz(:,j)  +Gau(i,j)*vij/sGau(i);
                end
            end            
        end

        Kst_iK = Ks_til*iK; % O(N³)
        if mni_mode                % terms from dMz, derivative w.r.t. K, Ks_til
            dlo_dK = zeros(N,N);
            dlo_dKs_til = zeros(N,N);
            traceBeta = 0;
            for i=1:N % compute leave-xi-out derivatives
                ni = [1:i-1,i+1:N]; % all other indices

                % compute iKni = inv(K(ni,ni)) and  Ani = iKni*Z(:,ni)'
                % efficiently through rank 1 updates
                ki = K(ni,i); vi = iK(ni,ni)*ki; ai = ki'*vi; wi = Z(:,ni)*vi;
                iKni = iK(ni,ni) - vi*(vi'/(K(i,i)+ai));                
                Ani = Alpha(ni,:) - iK(ni,i)*Z(:,i)' - vi*(wi'/(K(i,i)+ai));
                Bni = Ani*dlo_dMzni(:,ni,i);                       % O(N²D)
                traceBeta = traceBeta + trace(Bni);
                Cni = ( dlo_dMzni(:,ni,i)*Ks_til(ni,ni) )*iKni;
                dlo_dK(ni,ni) = dlo_dK(ni,ni) -Ani*Cni;
                dlo_dKs_til(ni,ni) = dlo_dKs_til(ni,ni) + Bni;
            end
        else
            Beta = Alpha*dlo_dMz; traceBeta = trace(Beta); % O(N²D)
            dlo_dK = -Beta*Kst_iK;   % terms from dMz, derivative w.r.t. K
            dlo_dKs_til = Beta;      % terms from dMz, derivative w.r.t. Ks_til
        end

%         % CODE FOR DERIVATIVE CHECKING
%         % deriv wrt Mz
%         [numgrad('dgplvmMzVz',1,1e-6,Mz,Vz,Z); dlo_dMz]
%         
%         % deriv wrt Vz
%         dlo_dVz2 = numgrad('dgplvmMzVz',2,1e-6,Mz,Vz,Z); 
%         [dlo_dVz2(:)'; dlo_dVz(:)']
% 
%         % deriv wrt Mzni
%         dlo_dMzni2 = numgrad('dgplvmMzVz',1,1e-6,Mzni,Vz,Z);
%         id = 3; [dlo_dMzni( :,:,id); dlo_dMzni2(:,:,id)]
%         keyboard
            
        % determine whether we have deterministic or stochastic inputs
        if ~numel(strfind(mode,'gauss'))
            if show_debug, fprintf('deterministic input\n'), end
            
            % apply chain rule to get derivative w.r.t. K
            dlo_dK = dlo_dK + dlo_dKs_til                   ... from dMz
                      +( (eye(N)-2*Kst_iK)*diag(dlo_dVz(:)) ... from dVz
                      + Kst_iK'*diag(dlo_dVz(:))*Kst_iK )';         
            
            if numel(strfind(mode,'dX'))     % chain rule to get derivs w.r.t. X
                dlo_dX = chainK_til(iW,nlatlen,sn,X,K,dlo_dK,'dX');
                % apply linear back constraints
                if lbc, dlo_dX = dlo_dX*Z'; end
                out2 = -dlo_dX;         % we need dnlo i.e. -dlo instead of dlo
            end
            if numel(strfind(mode,'hyp'))  % chain rule to get derivs w.r.t. hyp
                dlo_dhyp = chainK_til(iW,nlatlen,sn,X,K,dlo_dK,'dhyp');
                % correct for sn since nlo does not only depend on sn through K
                delta = 2*diag(Kst_iK)'*dlo_dVz(:) - traceBeta;
                dlo_dhyp(nlatlen+2) = dlo_dhyp(nlatlen+2) + delta*2*sn^2;
                if numel(strfind(mode,'dX'))
                    out2 = {out2,-dlo_dhyp};   % append dhyp if both are desired
                else
                    out2 = -dlo_dhyp;           % we need dnlo i.e. -dlo not dlo
                end
            end
        else
            if show_debug, fprintf('stochastic input\n'), end

            dlo_dK_hat = zeros(N,N,N);                % derivative w.r.t. K_hat            
            C = 0; Q = zeros(D,N); sum_tr_dVzl = 0;
            for l=1:N % terms from dVz
                dVzl = dlo_dVz(:,:,l); mzl = Mz(:,l);
                tr_dVzl = trace(dVzl);  sum_tr_dVzl = sum_tr_dVzl + tr_dVzl;
                % we recompute K_hat too often, yet more time is spent elsewhere
                [dKi, K_hat] = derivK_hat(W,nlatlen,sn,Vx,nlatvar,X,K,l);
                C = C + tr_dVzl*K_hat;
                AdA = Alpha*dVzl*Alpha';
                Pl = AdA*K_hat*iK;
                Q(:,l) = dVzl*mzl;
                dlo_dK = dlo_dK - (Pl+Pl') + 2*Alpha*Q(:,l)*Ks_til(:,l)'*iK;
                dlo_dK_hat(:,:,l) = AdA - tr_dVzl*iK;
            end
            dlo_dK = dlo_dK + iK*C*iK;
            dlo_dKs_til = dlo_dKs_til - 2*Alpha*Q;            
            
            if numel(strfind(mode,'dX'))     % chain rule to get derivs w.r.t. X
               % covariance matrix
                dlo_dX = chainK_til(inv(W),[],[],X,K,dlo_dK,mode);
                
                % twiddled covariance matrix
                dlo_dX = dlo_dX + ...
                         chainK_til(inv(Vx+W),[],[],X,Ks_til,dlo_dKs_til,mode);
                               
                % hatted covariance matrices
                K_hat = compAllK_hat(K,sn,Vx,X,iW);
                for l=1:N
                    B = dlo_dK_hat(:,:,l).*K_hat(:,:,l); C = B+B';
                    
                    % dl part
                    H = zeros(N,N); H(:,l) = sum(C,2); H(l,:) = sum(C,1); 
                    G = H-diag(sum(H,2));
                    dlo_dX = dlo_dX + inv(W)*X*G;
                    
                    % Dl part
                    H = diag(sum(C)) + C; J = H+H'; 
                    G = J; G(:,l) = G(:,l)-sum(J,2); G(l,:) = G(l,:)-sum(J,1);
                    G(l,l) = G(l,l) + sum(J(:));
                    dlo_dX = dlo_dX + inv(W*inv(2*Vx)*W+W)*X*(G'+G)/8;
                end
                % apply linear back constraints
                if lbc, dlo_dX = dlo_dX*Z'; end
                out2 = -dlo_dX;          % we need dnlo i.e. -dlo instead of dlo
            end
            if numel(strfind(mode,'hyp'))  % chain rule to get derivs w.r.t. hyp
                dlo_dhyp = zeros(nlatlen+2+nlatvar,1);
                for i=1:nlatlen+2+nlatvar
                    % covariance matrix
                    dKi = derivK_til(W,nlatlen,sn,0*Vx,nlatvar,X,K,i);
                    dlo_dhyp(i) = dlo_dhyp(i) + dlo_dK(:)'*dKi(:);
                    
                    % twiddled covariance matrix
                    dKs_til_i = derivK_til(W,nlatlen,0*sn,Vx,nlatvar,X,Ks_til,i);
                    dlo_dhyp(i) = dlo_dhyp(i) + dlo_dKs_til(:)'*dKs_til_i(:);
                    
                    % hatted covariance matrix
                    for l=1:N
                        dlo_dK_hat_l = dlo_dK_hat(:,:,l);
                        dKi = derivK_hat(W,nlatlen,sn,Vx,nlatvar,X,K,l,i);
                        dlo_dhyp(i) = dlo_dhyp(i) + dlo_dK_hat_l(:)'*dKi(:);
                    end
                end
                
                % correction for sf, sn since nlo does not only depend on them
                id = nlatlen+(1:2);               % through K, Ks_til and K_hat
                dlo_dhyp(id) = dlo_dhyp(id) + 2*sum_tr_dVzl*[sf;sn].^2;                
                if numel(strfind(mode,'dX'))
                    out2 = {out2,-dlo_dhyp};   % append dhyp if both are desired
                else
                    out2 = -dlo_dhyp;           % we need dnlo i.e. -dlo not dlo
                end                
            end
                        
        end
        if show_debug, fprintf('nlo=%1.3f.\n',out1), end
        % apply linear back constraints
        if lbc, out3 = P; else out3 = X; end
    end


%% density mode
elseif den_mode
    if size(XZstar,1)==d
        if show_debug, fprintf('projection mode\n'), end
        % project forward the test points
        [Mzstar,Vzstar] = projGauss(K,L,iK,Z,Alpha,sn,sf,iW,Vx,X,XZstar,mulVz,addVz);
        out1 = Mzstar; out2 = Vzstar;
    elseif size(XZstar,1)==D  
        % mixture of Gaussians in latent space
        [Mz,Vz,iVz,logdetVz] = projGauss(K,L,iK,Z,Alpha,sn,sf,iW,Vx,X,[],mulVz,addVz);        
        if show_debug, fprintf('density mode\n'), end
        log_Gau = evalMixture(Mz,iVz,logdetVz,XZstar);        % evaluate mixture
        lsGau = logsum2exp(log_Gau);
        out1 = lsGau -log(N); out2 = Mz; out3 = Vz;
    else
        error('wrong size of XZstar')
    end
end
return % from here onwards, only auxiliary functions follow, therefore terminate


%% compute the leave-out mask needed in leave-mode
% in every row of the mask the p-1 largest values of P and the diagonal are true 
function lomask = get_lomask(P,p)
    N = size(P,2);
    lomask = false(N);      % binary mask saying whom to leave out (val. scheme)
    for i=1:N        % mark the biggest entries, always include the diagonal one
        pi = P(i,:); pi(i) = Inf;     % always include the diagonal i.e. loo/p>0
        [ignore,id] = sort(pi,'descend'); 
        lomask(i,id(1:p)) = true;
    end
    
    
%% compute the leave-set-out mask needed in leave-mode
% in every row of the mask the p-1 largest values of P and the diagonal are true 
function lomask = get_lsomask(P,p, cluster)
    N = size(P,2);
    lomask = false(N);
    
    for i = 1:N
        for j = 1:N
            if i == j
                lomask(i,j) = true;
            end
            if cluster(i) == cluster(j)
                lomask(i,j) = true;
            end
        end
    end
    

%% extract hyperparameter configuration from cov and hyp
function [W,iW,nlatlen,sf,sn,Vx,nlatvar] = get_hyp(cov,hyp,d,mode)
    if numel(cov)==0, cov = {'covSum', {'covSEard','covNoise'}}; end   % default
    if strcmp(cov{1},'covSum') && strcmp(cov{2}(2),'covNoise')
        if strcmp(cov{2}(1),'covSEard'),     nlatlen = d; % # latent lengthscale
        elseif strcmp(cov{2}(1),'covSEiso'), nlatlen = 1;           % parameters
        else error('type of covariance function not supported'), end
        if numel(hyp)<nlatlen+2, error('not enough hyperparameters'), end    
        iW = diag(exp(-2*hyp(1:nlatlen)))*eye(d); % inv. sq. latent length scale
        sf = exp(hyp(nlatlen+1)); sn = exp(hyp(nlatlen+2));  % sig&noise std dev
        nlatvar = length(hyp)-nlatlen-2;  % number of latent variance parameters
        if nlatvar==0
            if numel(strfind(mode,'gauss'))             % check for 'gauss' mode
                Vx = eye(d);               % standard Gaussian input uncertainty
            else
                Vx = 0;                                   % no input uncertainty
            end
        elseif nlatvar==1 || nlatvar==d
            if numel(strfind(mode,'gauss'))             % check for 'gauss' mode
                dgVx = exp(2*hyp( nlatlen+2+(1:nlatvar) ));
                Vx = diag(dgVx)*eye(d);  % get diagonal latent covariance matrix
            else
                error('wrong number of hyperparameters')
            end
        else
            error('wrong number of hyperparameters')
        end
    else
        error('type of covariance function not supported')
    end
    W = inv(iW);


%% chain rule for to compute df/d{X,hyp} from df/dK for deterministic inputs
function df = chainK_til(iW,nlatlen,sn,X,K,df_dK,mode)
    % calculate derivatives w.r.t. hyperparameters hyp
    if numel(strfind(mode,'dhyp'))
        df = zeros(nlatlen+2,1);
        W = diag(1./diag(iW));
        for i=1:length(df)
            % dKi = feval(cov{:},hyp,X',i);            
            dKi = derivK_til(W,nlatlen,sn,0*W,0,X,K,i);
            % chain rule to get derivative w.r.t. hyp 
            df(i) = df_dK(:)'*dKi(:);
        end
    % calculate derivatives w.r.t. latent parameters X
    elseif numel(strfind(mode,'dX'))
        % chain rule to get derivative w.r.t. X
         B = df_dK.*K; C = B+B';
        df = iW*X*(C-diag(sum(C)));
    else
        df = [];
    end


%% derivative of K and K_til w.r.t. individual hyper-parameters
% dK/dln(sn), dK/dln(sf), dK/dln(sqrt(W(i))), dK/dln(sqrt(Vx(i))), works for
% K_til = sn^2*eye(N) + sf2_til*exp(-sq_dist(chol(inv(W_til))*X)/2);
%    where sf2_til = sf^2 / sqrt(det( Vx*inv(W)+eye(d) ));  and  W_til = Vx+W;
% as well as
% K = sn^2*eye(N) + sf^2*exp(-sq_dist(chol(inv(W))*X)/2); for Vx=0
function dKi = derivK_til(W,nlatlen,sn,Vx,nlatvar,X,K,i)
    [d,N] = size(X);    
    if i<=nlatlen || nlatlen+2<i && i<=nlatlen+2+nlatvar % length scales, vars
        Vx = eye(d)*Vx; W = eye(d)*W;    % sanity to make sure, we have matrices
        Ks = K-sn^2*eye(N);
        w_til = diag(Vx+W);   vx = diag(Vx);           % w_til equals w for Vx=0
        
        if i<=nlatlen
            R = diag( sqrt(diag(W))./w_til );  sign = +1; % covSEiso
            if nlatlen>1
                Ei = zeros(d); Ei(i,i) = 1; R = R*Ei;     % covSEard
                vx = vx(i); w_til = w_til(i);
            end
        else
            R = diag( sqrt(diag(Vx))./w_til ); sign = -1; % covSEiso
            if nlatvar>1
                ii = i-nlatlen-2;
                Ei = zeros(d); Ei(ii,ii) = 1; R = R*Ei;   % covSEard
                vx = vx(ii); w_til = w_til(ii);
            end     
        end
        dKi = Ks.*( sq_dist(R*X) + sign*sum(vx./w_til) );
    elseif i==nlatlen+1 % noise variance
        dKi = 2*(K-sn^2*eye(N));
    elseif i==nlatlen+2 % signal variance
        dKi = 2*sn^2*eye(N);
    else
        dKi = zeros(N);
    end


%% compute the stack of all K_hat
function K_hat = compAllK_hat(K,sn,Vx,X,iW)
    [d,N] = size(X);
    css = 1/sqrt(det( 2*Vx*iW+eye(d) ));
    try R = chol(inv(inv(iW)/2+Vx)*Vx*iW); catch, R = 0; end % allow for Vx -> 0
    Ks = K-sn^2*eye(N);
    K_hat = zeros(N,N,N);
    for i=1:N
        mxi = X(:,i); ksi = Ks(:,i);
        % compute diagonal terms of output covariance Vz     
        inp  = R*(X-repmat(mxi,1,N))/2;
        K_hat(:,:,i) = css*(ksi*ksi').*exp(sq_dist(inp,-inp)); % ksi*ksi', Vx->0   
    end    
    

%% derivative of K_hat w.r.t. individual hyper-parameters
% dK/dln(sn), dK/dln(sf), dK/dln(sqrt(W(i))), dK/dln(sqrt(Vx(i))), works for
% K_hat is needed for the computation of the predictive covariance matrix
function  [dKi, K_hat] = derivK_hat(W,nlatlen,sn,Vx,nlatvar,X,K,l,i)
    [d,N] = size(X);

    w_til = diag( W*inv(Vx)*W/2+W ); % Inf for Vx -> 0   
    w_hat = diag( 2*inv(W)*Vx+eye(d) );
    css = 1/sqrt(det(diag(w_hat)));
    R = chol(diag(1./w_til));
    
    ksl = K(:,l); ksl(l) = ksl(l) - sn^2; mxl = X(:,l); 
    inp = R*(X-repmat(mxl,1,N))/2;
    K_hat = css*(ksl*ksl').*exp(sq_dist(inp,-inp));    
    
    if nargin==8, dKi=0; return, end
    
    if i<=nlatlen % length scales
        Vx = eye(d)*Vx; W = eye(d)*W;    % sanity to make sure, we have matrices
        vx = diag(Vx); w = diag(W);       
        on = ones(N,1); % vector of ones
        R_til = diag(sqrt( (4*w_til-2*w)./w_til) ); R = diag(1./sqrt(w));
        if nlatlen>1
            Ei = zeros(d); Ei(i,i) = 1; % covSEard
            R_til = R_til*Ei; R = R*Ei; vx = vx(i); w = w(i);
        end
        D2_til = sq_dist(R_til*inp,-R_til*inp);
        d2l = sq_dist(R*X,R*X(:,l)); % for derivs of ksl
        dKi = K_hat.*(  d2l*on'+on*d2l'  -D2_til  +sum(vx./(vx+w/2)) );
    elseif i==nlatlen+1 % sf, simple
        dKi = 4*K_hat;
    elseif i==nlatlen+2 % sn, 0
        dKi = zeros(N);
    elseif nlatlen+2<i && i<=nlatlen+2+nlatvar % Vx
        Vx = eye(d)*Vx; W = eye(d)*W;          % make sure, we have matrices
        vx = diag(Vx); w = diag(W);
        R_hat = chol(diag(2./w_hat)); sign = -1; % covSEiso
        if nlatvar>1
            ii = i-nlatlen-2;
            Ei = zeros(d); Ei(ii,ii) = 1; R_hat = R_hat*Ei; % covSEard
            vx = vx(ii);  w = w(ii);
        end

        D2_til = sq_dist(R_hat*inp,-R_hat*inp);
        dKi = K_hat.*( D2_til + sign*sum(vx./(vx+w/2)) );            
    else
        dKi = zeros(N);
    end    
    

%% a mixture of Gaussians specified by (Mz,Vz) is evaluated for points Zstar
function log_Gau = evalMixture(Mz,iVz,logdetVz,Zstar)
    [D,Nstar] = size(Zstar); N = size(Mz,2);
    % log density of the Gaussian in z space
    log_Gau = -D*log(2*pi)/2 -ones(Nstar,1)/2*logdetVz';
    if ndims(Mz)>2 % mni mode
        if Nstar~=N, error('Zstar has to equal Z in mni mode'), end
        for i=1:Nstar
            for j=[1:i-1,i+1:N]
                % predictive mean is mz, variance is Vz
                zi_mzj = Zstar(:,i)-Mz(:,j,i);
                vij = iVz(:,:,j)*zi_mzj; % inv(Vzj)*(zi-mz)
                log_Gau(i,j) = log_Gau(i,j) -(zi_mzj'*vij )/2;
            end
            log_Gau(i,i) = -Inf;
        end   
    else
        for i=1:Nstar % O(N Nstar D²)
            for j=1:N
                % predictive mean is mz, variance is Vz
                zi_mzj = Zstar(:,i)-Mz(:,j);
                vij = iVz(:,:,j)*zi_mzj; % inv(Vzj)*(zi-mz)
                log_Gau(i,j) = log_Gau(i,j) -(zi_mzj'*vij )/2;
            end
        end
    end

%% computes y = log(sum(exp(x),2)) in a numerically safe way by subtracting the
%  row maximum to avoid cancelation after taking the exp
%  the sum is done along the rows
%  the output is not exactly exp(logx) but a constant factor away from it
function [y,x] = logsum2exp(logx)
    N = size(logx,2);
    max_logx = max(logx,[],2);
    % we have all values in the log domain, and want to calculate a sum
    x = exp(logx-max_logx*ones(1,N));
    y = log(sum(x,2)) + max_logx;


%% projection of Gauss data through the GPLVM 
% It takes O(N³) for deterministic inputs Xstar and O(N³D) for stochastic
% to compute means, covariances, inverses logdet and kernel matrices
% the covariances in the case of Vx~=0 (non-spherical case) can be scaled and 
% shifted i.e. Vz <- Vz.*mulVz + addVz, this is taken care of in iVz & logdetVz
function [Mz,Vz,iVz,logdetVz,Ks_til] = ...
                          projGauss(K,L,iK,Z,Alpha,sn,sf,iW,Vx,X,Xs,mulVz,addVz)

    [D,N] = size(Z); d = size(X,1); W = diag(1./diag(iW));
    Xs_eq_X = nargin<11;                             % flag saying whether X==Xs
    if nargin>=11, Xs_eq_X = ~numel(Xs); end   % if Xs is empty then it equals X
    if Xs_eq_X
        % save some computation, since Ks is trivial to get in the case of Xs==X
        Ks = K-sn^2*eye(N);                   % cross covariance w/o noise stuff
    else
        % covariance evaluation: [kss, Ks] = feval(cov{:}, hyp, X',Xs');
        R = chol(W)\eye(d);
        Ks = sf^2*exp(-sq_dist(R*X,R*Xs)/2);
    end
    kss = sf^2+sn^2;
    
    %% predictions for deterministic inputs
    if norm(Vx)==0
        Mz = Alpha'*Ks; % all DxN predictive means
        % all diagonal covariances at once: diag(Ks-Ks*inv(K)*Ks)
        v = L'\Ks;
        Vz = reshape( kss - sum(v.*v), 1,1,[] );
        % compute log(det(Vz)) and inv(Vz)
        logdetVz = D*log(Vz(:)); iVz = 1./Vz;
        Ks_til = Ks;
        return
    end
   
    %% predictions for stochastic inputs (save when Vx is low rank or zero)
    sf2_til = sf^2 / sqrt(det( Vx*iW+eye(d) ));           % equals sf^2 for Vx=0
    if Xs_eq_X, Xs=X; end, Ns = size(Xs,2);
    R = chol(Vx+W)\eye(d);
    Ks_til = sf2_til*exp(-sq_dist(R*X,R*Xs)/2);             % equals Ks for Vx=0
    Mz = Alpha'*Ks_til; % all DxN predictive means
           
    %% stochastic variance prediction  
    Vz = zeros(D,D,Ns); iVz = zeros(D,D,Ns); logdetVz = zeros(Ns,1);  % allocate
    css = 1/sqrt(det( 2*Vx*iW+eye(d) ));
    try R = chol(inv(W/2+Vx)*Vx*iW); catch, R = 0; end       % allow for Vx -> 0
    for i=1:Ns
        mxi = Xs(:,i); mzi = Mz(:,i); ksi = Ks(:,i);
        % compute diagonal terms of output covariance Vz     
        inp  = R*(X-repmat(mxi,1,N))/2;
        K_hat_i = css*(ksi*ksi').*exp(sq_dist(inp,-inp)); % ksi*ksi' for Vx -> 0        
        % the non-diagonal part is exactly zero for Vx=0 and the spherical part 
        % equals kss-trace(iK*K_hat_i) as in the deterministic case 
        Vz(:,:,i) = ( kss-sum(sum(iK.*K_hat_i)) )*eye(D)     ...  spherical part
                    + Alpha'*K_hat_i*Alpha -mzi*mzi';        % non-diagonal part
        Vz(:,:,i) = (Vz(:,:,i)+Vz(:,:,i)')/2;           % numerically symmetrize
        if nargin>11, Vz(:,:,i) = Vz(:,:,i).*mulVz; end  % multiplic. term to Vz
        if nargin>12, Vz(:,:,i) = Vz(:,:,i)+addVz; end     % additive term to Vz
        if nargout>2
            R2 = chol(Vz(:,:,i)); 
            logdetVz(i) = 2*sum( log(diag(R2)) );
            iVz(:,:,i) = R2\(R2'\eye(D));
        end
%         % compute input output covariances Vxz, handle Vx == 0 correctly
%         Vxz = Vx*inv(Vx+W)*(X-repmat(mxi,1,N)) * (repmat(Ks_til(:,i),1,D).*Alpha);
%         % approximate covariances in z space, the second one is more accurate
%         Vz1 = Vxz'*(Vx\Vxz) + sn^2*eye(D); 
%         Vz2 = Vz1 + (sf^2-ksi'*iK*ksi) * eye(D); 
    end
