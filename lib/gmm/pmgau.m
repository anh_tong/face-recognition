function [lp,lpte,w] = pmgau(X, y, Xte)

% PMGAU Penalized Mixture of Gaussian Density Estimation.
%  fit a mixture of regularized Gaussians to the data, the mean is the class
%  sample mean, the weight of mixture components is proportional to the number
%  of training examples falling into that component
%     m = mean(X,2) 
%  and the covariance equals the class 
%     C = (X*X' -N*m*m' + w^2*eye(D))/(N+1)
%  The regularization parameter is found by gradient descent on the
%  leave-one-out likelihood of the training data.
%
%  usage: [lp,lpte,w] = pmgau(X, y, Xte)
%     inputs:  X    (D by N  ) matrix of training points in D dimensions
%              y    (N)        vector of training point labels
%              Xte  (D by Nte) matrix of test points in D dimensions
%     outputs  lp   (N   by 1) vector of log densities of X
%              lpte (Nte by 1) vector of log densities of Xte
%              w    (1 by 1)   regularization parameter
%
%    (C) Copyright Hannes Nickisch, May 19th 2009.

% analyze the number of clusters in the training data
[yu,ignore,id] = unique(y); % yu(id) == y
Nc = numel(yu); N = length(y);
for i=1:Nc, h(i)=sum(id==i)/N; end % compute a normalized histogram
if min(h)<1e-2 || Nc>N/4
    fprintf('    => switched to PGAU due to heavy imbalance\n')
    [lp,lpte,w] = pgau(X, Xte);
    return
end

% break the data set into Nc classes
for i=1:Nc, Xc{i} = X(:,id==i); end
w = sqrt(mean(diag(cov(X')))); % average variance yields a ballpark
[txt,w] = evalc('minimize(w, @nmlpgauloo, -20, Xc)'); % do 20 CG steps

% density of training cases depends on their class membership
lp = -Inf(N,1);
for i=1:Nc % iterate over the classes
    lp(id==i) = lpgauloo(w,Xc{i});
end

% density of test cases comes from a mixture
lpte = -Inf(size(Xte,2),Nc);
for i=1:Nc % iterate over the classes
    a = h(i); % weighting according to training classes
    % a = 1/Nc; % equal weighting
    lpte(:,i) = lpgau(w,Xc{i},Xte)+log(a); % predict under all classes
end
lpte = logsum2exp(lpte); % combine


%% mean negative log LOO probabilities wrapper function
function [nmlp,dnmlp] = nmlpgauloo(w,Xc)
    nmlp = 0;
    for i=1:length(Xc) % sum over the classes
        nmlp = nmlp - mean(lpgauloo(w,Xc{i}));
    end
    if nargout>1
        % finite difference approximation to the gradient
        h = 1e-4; dw = w*h;
        nmlp_d = 0;
        for i=1:length(Xc) % sum over the classes
            nmlp_d = nmlp_d - mean(lpgauloo(w+dw,Xc{i}));
        end
        dnmlp = (nmlp_d - nmlp)/dw;
    end

    
%% log LOO probabilities
function lp = lpgauloo(w,X)
    [D,N] = size(X); w2 = abs(w^2);
    m = sum(X,2);                    % m/N is an unbiased mean estimator
    A = X*X' + w2*eye(D);
    iA = inv(A); ldA = logdet(A);    % inverse and log-determinant of A
    lp = -Inf(N,1); % LOO train log probabilities
    for i=1:N
        xi = X(:,i);
        m_ni = (m-xi)/(N-1); % mean with xi removed
        xi_m = xi-m_ni;
        % norm( mean(X(:,[1:i-1,i+1:N]),2)-m_ni )

        % compute iA_ni and ldA_ni with xi removed through rank 1 updates
        v = iA*xi; % A_ni = X*X' -xi*xi' + w2*eye(D);
        iA_ni  = iA - v/(v'*xi-1)*v'; ldA_ni = ldA + log(1-v'*xi);
        % A_ni = X*X' -xi*xi' + w2*eye(D);
        % [norm(inv(A_ni)-iA_ni,'fro'), norm(logdet(A_ni)-ldA_ni)]

        % compute iC_ni and ldC_ni with xi removed through rank 1 updates
        v = iA_ni*m_ni; % C_ni = X*X' - xi*xi' - m_ni*m_ni' + w2*eye(D);
        iC_ni  = iA_ni - v/(v'*m_ni-1)*v'; ldC_ni = ldA_ni + log(1-v'*m_ni);
        % C_ni = X*X' - xi*xi' - m_ni*m_ni' +w2*eye(D);
        % [norm(inv(C_ni)-iC_ni,'fro'), norm(logdet(C_ni)-ldC_ni)]

        lp(i) = -D*log(2*pi/(N-2))/2 -ldC_ni/2 -(xi_m'*iC_ni*xi_m)*(N-2)/2;
    end

    
%% evaluate ordinary log probabilities on test set
function lpte = lpgau(w,X,Xte)
    [D,Nte] = size(Xte); N = size(X,2); w2 = abs(w^2);
    m = sum(X,2);                    % m/N is an unbiased mean estimator
    C = X*X' - m*m'/N + w2*eye(D);    % C/(N-1) is a regularized cov. estimator
    iC = inv(C); ldC = logdet(C);    % inverse and log-determinant of C
    lpte = -Inf(Nte,1); 
    for i=1:Nte
        xi_m = Xte(:,i)-m/N;
        lpte(i) = -D*log(2*pi/(N-1))/2 -ldC/2 -(xi_m'*iC*xi_m)*(N-1)/2;
    end

    
%% log(det(A)) function
function ldA = logdet(A)
    ldA = 2*sum(log(diag(chol(A))));

%% computes y = log(sum(exp(x),2)) in a numerically safe way by subtracting the
%  row maximum to avoid cancelation after taking the exp
%  the sum is done along the rows
function [y,x] = logsum2exp(logx)
    N = size(logx,2);
    max_logx = max(logx,[],2);
    % we have all values in the log domain, and want to calculate a sum
    x = exp(logx-max_logx*ones(1,N));
    y = log(sum(x,2)) + max_logx;
    