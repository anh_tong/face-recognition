function [lp,lpte,w] = pgmm(X, Xte, k, p, seed)

% PGMM Penalized Gaussian Mixture Model.
%  fit a mixture of regularized Gaussians to the data, whose centers are
%  determined by k-means and whose variances are computed by pmgau
%
%  the k-means initialisation is randn (repeated p times) i.e. it makes sense 
% to have properly scaled data
%
%  usage: [lp,lpte,w] = pgmm(X, Xte, k, p, seed)
%     inputs:  X    (D by N  ) matrix of training points in D dimensions
%              Xte  (D by Nte) matrix of test points in D dimensions
%              k    (1 by 1)   number of mixture components   [default 5]
%              p    (1 by 1)   number of random k-means inits [default 10]
%     outputs  lp   (N   by 1) vector of log densities of X
%              lpte (Nte by 1) vector of log densities of Xte
%              w    (1 by 1)   regularization parameter
%
%    (C) Copyright Hannes Nickisch, September 21st 2009.

if nargin>4, randn('seed',seed); end % use pseudo randomness, to shuffle dataset
if nargin<4, p = 10; end
if nargin<3, k =  5; end

% repeated k-means using EM
fbest = Inf; [d,n] = size(X);
for i=1:p
    [out1,out2,out3,ignore,f] = kmeans(X',randn(k,d));
    if f<fbest
        mu = out1; muid = out2; mudist = out3; fbest = f;
    end
end

% evaluate both training and test data
[lp,lpte,w] = pmgau(X, muid, [X,Xte]);
lp = lpte(1:n); lpte = lpte(n+1:end);
