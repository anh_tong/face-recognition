% demo program for the gmm toolbox by Hannes Nickisch, 2010 August 27
clear all, close all, addpath data gpml kmeans util

[z,y,zte,yte] = breast_load;                 % load Wisconsin Breast Cancer data

alg = {'kde', 'mkde', 'ikde', 'dkde', 'mpar', 'pgau', 'pgmm', 'pmgau',...
        'lZ', 'l2o', 'l2o-gau'};

% 1) kernel density estimation
mlp = mean(kde(z',z')); mlpte = mean(kde(z',zte'));

% 2) mixture of kernel density estimators (class-weighted)
[lp,lpte] = mkde(z,y,zte); mlp(2) = mean(lp); mlpte(2) = mean(lpte);

% 3) isotropic kernel density estimation
[lp,lpte] = ikde(z,zte); mlp(3) = mean(lp); mlpte(3) = mean(lpte);

% 4) diagonal kernel density estimation
[lp,lpte] = dkde(z,zte); mlp(4) = mean(lp); mlpte(4) = mean(lpte);

% 5) manifold Parzen windows
d = 2; k = 5; % number of eigs, number of nearest neighbors
[lp,lpte] = mpar(z,zte,d,k); mlp(5) = mean(lp); mlpte(5) = mean(lpte);

% 6) single penalised Gaussian
[lp,lpte] = pgau(z,zte); mlp(6) = mean(lp); mlpte(6) = mean(lpte);

% 7) penalised mixture of Gaussians, k-means center
k = 5; % number of mixture components
[lp,lpte] = pgmm(z,zte,k); mlp(7) = mean(lp); mlpte(7) = mean(lpte);

% 8) penalised mixture of Gaussians, class mean center
[lp,lpte] = pmgau(z,y,zte); mlp(8) = mean(lp); mlpte(8) = mean(lpte);

% 9) DGPLVM using marginal likelihood optimised centers x and hypers hyp
fprintf('dgplvm lZ\n')
fprintf('\ngplvm marginal likelihood with latent Dirac\n')
cov = {'covSum', {'covSEiso','covNoise'}}; % covariance function
len = 0.25;  % lengthscales
sf  =  0.25;  % signal standard deviation
sn  =  0.02;  % noise standard deviation
mode = '';
hyp0 = log([len; sf; sn]);
d = 2; x0 = gplvmSE(hyp0, cov, zeros(d,0), z); % PCA init
x = x0; hyp = hyp0; % init params
for i=1:3
  md = ['dhyp ',mode];
  [txt,hyp,fhyp] = evalc('minimize(hyp,''gplvmSE'',-15,cov,x,z,md)');
  fprintf('fhyp=%1.3f',fhyp(end))
  md = ['dX ',mode];
  [txt,x,fx] = evalc('minimize(x(:),''gplvmSEwrapX'',-30,hyp,cov,z,md)'); 
  x=reshape(x,d,[]);
  fprintf(', fx=%1.3f\n',fx(end))
end
lp = gplvmSE(hyp,cov,x,z,mode,z); lpte = gplvmSE(hyp,cov,x,z,mode,zte);
mlp(9) = mean(sum(lp,2)); mlpte(9) = mean(sum(lpte,2));

% 10) DGPLVM using leave-2-out optimised centers x and hypers hyp
fprintf('\ndgplvm leave-2-out with latent Dirac\n')
x = x0; hyp = hyp0; % init params
mode = 'l2o mni';
for i=1:2
  md = ['dhyp ',mode];
  [txt,hyp,fhyp] = evalc('minimize(hyp,''gplvmSE'',-15,cov,x,z,md)');
  fprintf('fhyp=%1.3f',fhyp(end))
  md = ['dX ',mode];
  [txt,x,fx] = evalc('minimize(x(:),''gplvmSEwrapX'',-30,hyp,cov,z,md)'); 
  x=reshape(x,d,[]);
  fprintf(', fx=%1.3f\n',fx(end))
end
lp = gplvmSE(hyp,cov,x,z,mode,z); lpte = gplvmSE(hyp,cov,x,z,mode,zte);
mlp(10) = mean(sum(lp,2)); mlpte(10) = mean(sum(lpte,2));

% 11) DGPLVM using leave-2-out optimised centers x and hypers hyp
fprintf('\ndgplvm leave-2-out with latent Gaussian\n')
vx = 0.01; hyp0 = log([len; sf; sn; sqrt(vx)]);
x = x0; hyp = hyp0; % init params
mode = 'l2o mni gauss';
for i=1:2
  md = ['dhyp ',mode];
  [txt,hyp,fhyp] = evalc('minimize(hyp,''gplvmSE'',-15,cov,x,z,md)');
  fprintf('fhyp=%1.3f',fhyp(end))
  md = ['dX ',mode];
  [txt,x,fx] = evalc('minimize(x(:),''gplvmSEwrapX'',-30,hyp,cov,z,md)'); 
  x=reshape(x,d,[]);
  fprintf(', fx=%1.3f\n',fx(end))
end
lp = gplvmSE(hyp,cov,x,z,mode,z); lpte = gplvmSE(hyp,cov,x,z,mode,zte);
mlp(11) = mean(sum(lp,2)); mlpte(11) = mean(sum(lpte,2));

for i=1:11
  fprintf('%s: train density %1.3f, test density %1.3f\n',...
    alg{i},mlp(i),mlpte(i))
end