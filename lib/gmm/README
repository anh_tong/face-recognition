gmm: Gaussian Mixture Modeling with 
     Gaussian Process Latent Variable Models and Others
     http://mloss.org/software/view/272/

The gmm toolbox contains code for density estimation using mixtures of Gaussians. Starting from simple kernel density estimation using spherical and diagonal Gaussian kernels over manifold Parzen window until mixtures of penalised full Gaussians with only a few components the toolbox covers many Gaussian mixture models parametrisation from the recent literature.

Most prominently, the package contains code to use the Gaussian Process Latent Variable Model for density estimation as described in the paper by Hannes Nickisch and Carl E. Rasmussen http://arxiv.org/pdf/1006.3640.

Most of the code is written in Matlab 7.x including some MEX files.

(c) Hannes Nickisch, MPI for Biological Cybernetics, August 27th 2010

CODE ORGANISATION
=================

a) DIRECTORIES
--------------
data/   : Wisconsin Breast Cancer data from UCI Machine Learning Repository
kmeans/ : k-means clustering code by Charles Elkan
          http://cseweb.ucsd.edu/~elkan/fastkmeans.html
util/   : some wrapper functions for the optimisation process
gpml/   : some auxiliary function for Gaussian processes taken from the
          GPML toolbox by Carl E. Rasmussen and Hannes Nickisch
          http://www.gaussianprocess.org/gpml/code
          Some code can be accelerated by compiling the MEX-files using
          the script gpml/make.m from the Matlab shell.

b) COPYRIGHT
------------
The code is published under the FreeBSD license, see Copyright.

c) DENSITY ESTIMATION
---------------------
gplvmSE : Gaussian process latent variable model with squared exponential
          covariance functio
kde.m   : kernel density estimation (by Carl E. Rasmussen)
          http://www.kyb.tuebingen.mpg.de/bs/people/carl/code/kde/
mkde.m  : mixture of kde (several instances of kde)
ikde.m  : isotropic KDE
dkde.m  : diagonal KDE (one ikde instance per dimension)
mpar.m  : Manifold Parzen Windows, Vincent and Bengio, NIPS 2002
          http://www.iro.umontreal.ca/~vincentp/Publications/mparzen_nips2002.pdf
pgau.m  : penalised Gaussian
pgmm.m  : penalised Gaussian mixture (centers are determined by kmeans, 
                                      then pmgau is executed)
pmgau.m : mixture of penalised Gaussians (several instances of pgau sharing 
                                          the same penalty parameter)

d) DEMO
-------
demo_gmm.m

e) PAPER
--------
nickisch10dgplvm.pdf
