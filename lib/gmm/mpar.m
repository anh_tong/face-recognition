function [lp,lpte,s2] = mpar(X, Xte, d, k)

%  Manifold Parzen Window density estimation as proposed by 
%  Vincent & Bengio in NIPS 2002
%
%  usage: [lp,lpte,w] = dkde(X, Xte, d, k)
%     inputs:  X    (D by N  ) matrix of training points in D dimensions
%              Xte  (D by Nte) matrix of test points in D dimensions
%     outputs  lp   (N   by 1) vector of log densities of X
%              lpte (Nte by 1) vector of log densities of Xte
%              d    (1 by 1)   number of eigenvectors of the cov. matrices
%              k    (1 by 1)   number of nearest neighbors used
%
%    (C) Copyright Hannes Nickisch, June 2009.
%        last modification September 22th 2009

if nargin<4, k = 10; end % number of nearest neighbors

[D,N] = size(X); % dimension of the data

% evaluate k-nearest neighbor kernel
D2 = sq_dist(X); for i=1:N, D2(i,i)=Inf; end, K = zeros(N,N);
[ignore,id] = sort(D2,'ascend');
for i=1:N, K(id(1:k,i),i) = 1; end
% set diagonal entries to zero
K = K-diag(diag(K));
% normalize the columns: sum(K) = ones(1,N) => yields probabilities
K = K./repmat(sum(K),[N,1]);

% compute the N covariance local representations Ci = w²*eye(D) + Vi*Li*Vi';
op.disp = 0; V = zeros(D,d,N); L = zeros(d,N);
for i=1:N
    X_xi = bsxfun(@minus,X,X(:,i));
    Ci = X_xi*diag(K(:,i))*X_xi';     % O(ND²)
    [Ui,Ei] = eigs(Ci,d,'lm',op);     % O(D³)
    V(:,:,i) = Ui; L(:,i) = diag(Ei); % store rank d stuff
end

s2 = mean(diag(cov(X')));        % average variance yields a ballpark for w
[txt,w] = evalc('minimize(sqrt(s2), @nmlpmparloo, -20, X, V, L, d)');  % do 20 CG steps
s2 = w^2;

lp   = lpmpar(X, V, L, X,   d, s2);
lpte = lpmpar(X, V, L, Xte, d, s2);


%% mean negative log
function [nmlp,dnmlp] = nmlpmparloo(w, X, V, L, d)
    nmlp = -mean(lpmparloo(X,V,L,d,w^2));
    if nargout>1
        % finite difference approximation to the gradient
        h = 1e-4; dw = w*h;
        dnmlp = (-mean(lpmparloo(X,V,L,d,(w+dw)^2)) - nmlp)/dw;
    end


%% log LOO probabilities
function lpte = lpmparloo(M, V, L, d, s2)
    [D,N] = size(M); s2 = abs(s2);
    % evaluate log-density of data under model
    logdet = D*log(2*pi) + (D-d)*log(s2) + sum(log(L+s2),1); % log(det(2*pi*Ci))
    sqdist = zeros(N,N);   % a column corresponds to densities of a single point
    for i=1:N % predict each point using the mixture
        mi = M(:,i);
        X_mi = bsxfun(@minus,M,mi);    % sqdist(:,i) = diag(X_mi'*inv(Ci)*X_mi);
        sqdist(:,i) = sum(X_mi.^2)/s2 ...
                                   -(1/s2 -1./(L(:,i)+s2))'*(V(:,:,i)'*X_mi).^2;
    end
    lpte = -bsxfun(@plus,logdet,sqdist)/2 -log(N);
    for i=1:N, lpte(i,i)=-Inf; end
    lpte = logsum2exp(lpte);    
    

%% evaluate ordinary log probabilities on test set
function lpte = lpmpar(M, V, L, Xte, d, s2)
    [D,N] = size(M); Nte = size(Xte,2);
    % evaluate log-density of data under model
    logdet = D*log(2*pi) + (D-d)*log(s2) + sum(log(L+s2),1); % log(det(2*pi*Ci))
    sqdist = zeros(Nte,N); % a column corresponds to densities of a single point
    for i=1:N % predict each point using the mixture
        mi = M(:,i);
        X_mi = bsxfun(@minus,Xte,mi);  % sqdist(:,i) = diag(X_mi'*inv(Ci)*X_mi);
        sqdist(:,i) = sum(X_mi.^2)/s2 ...
                                   -(1/s2 -1./(L(:,i)+s2))'*(V(:,:,i)'*X_mi).^2;
    end
    lpte = logsum2exp( -bsxfun(@plus,logdet,sqdist)/2 -log(N) );


%% computes y = log(sum(exp(x),2)) in a numerically safe way by subtracting the
%  row maximum to avoid cancelation after taking the exp
%  the sum is done along the rows
function [y,x] = logsum2exp(logx)
    N = size(logx,2);
    max_logx = max(logx,[],2);
    % we have all values in the log domain, and want to calculate a sum
    x = exp(logx-max_logx*ones(1,N));
    y = log(sum(x,2)) + max_logx;
