% wrapper for gplvmSE, needed for minimization in X
function [val,dval] = gplvmSEwrapX(X, hyp, cov, Z, mode, varargin)

% do a reshape in X
[D,N] = size(Z);
if numel(strfind(mode,'lbc')), sz = D; else sz = N; end
if mod(numel(X),sz)
    error('X does not have the right size')
else
    X = reshape(X,[],sz);
end

% call the function
[val,dval] = gplvmSE(hyp, cov, X, Z, mode, varargin{:});  dval = dval(:);