function [lp,lpte,w] = dkde(X, Xte, w)

% Diagonal Kernel Density Estimation i.e. diagonal covariance diag(w.^2)
%  The regularization parameter w is found by gradient descent on the
%  leave-one-out likelihood of the training data.
%
%  usage: [lp,lpte,w] = dkde(X, Xte, w)
%     inputs:  X    (D by N  ) matrix of training points in D dimensions
%              Xte  (D by Nte) matrix of test points in D dimensions
%     outputs  lp   (N   by 1) vector of log densities of X
%              lpte (Nte by 1) vector of log densities of Xte
%              w    (1 by D)   vector or regularization parameters
%
%    (C) Copyright Hannes Nickisch, September 22th 2009.
%        last modification September 22th 2009

d = size(X,1);
if nargin<3
    w = zeros(d,1);
    for i=1:d
        w(i) = mean(mean(sqrt(sq_dist(X(i,:)))));    % avg sq dist is a ballpark
    end
end
w = abs(w(:)).*ones(d,1);

for i=1:d
    % remove double occurences
    [txt,w(i)] = evalc('minimize(w(i), @dnloodensScal, -30, unique(X(i,:)))'); % do 30 CG steps
end
% train and test density
lp = loodens(X,w);  lpte = dens(X,Xte,w);

%% computes negative log LOO train density AND derivative
function [nmlp,dnmlp] = dnloodensScal(w,X)
    nmlp = -mean(loodens(X,w));
    if nargout>1
        % finite difference approximation to the gradient
        h = 1e-4; dw = abs(w)*h;
        dnmlp = (-mean(loodens(X,w+dw)) - nmlp)/dw;
    end


%% computes log LOO train density
function lp = loodens(X,w)
    [d,n] = size(X); w = w.*ones(d,1); iw = 1./w;
    lp   = -d/2*log(2*pi) -sum(log(w.^2))/2 -sq_dist( X.*(iw*ones(1,n)) )/2;
    lp   = logsum2exp( lp - diag(Inf(n,1)) ) - log(n);


%% computes log test density
function lpte = dens(X,Xte,w)
    [d,n] = size(X); w = w.*ones(d,1); iw = 1./w; nte = size(Xte,2);
    lpte = -d/2*log(2*pi) -sum(log(w.^2))/2 -sq_dist( Xte.*(iw*ones(1,nte)), X.*(iw*ones(1,n)) )/2;
    lpte = logsum2exp(lpte) - log(n);


%% computes y = log(sum(exp(x),2)) in a numerically safe way by subtracting the
%  row maximum to avoid cancelation after taking the exp
%  the sum is done along the rows
function [y,x] = logsum2exp(logx)
    N = size(logx,2);
    max_logx = max(logx,[],2);
    % we have all values in the log domain, and want to calculate a sum
    x = exp(logx-max_logx*ones(1,N));
    y = log(sum(x,2)) + max_logx;
