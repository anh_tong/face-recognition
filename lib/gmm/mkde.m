function [lp,lpte,s] = mkde(X, y, Xte)

% MKDE Mixture of Gaussian Kernel Density Estimators.
%  individually fit a KDE per class, use number of training examples of the
%  class as mixture weight
%
%  usage: [lp,lpte,w] = kde(X, y, Xte, yte)
%     inputs:  X    (D by N  ) matrix of training points in D dimensions
%              y    (N)        vector of training point labels
%              Xte  (D by Nte) matrix of test points in D dimensions
%     outputs  lp   (N   by 1) vector of log densities of X
%              lpte (Nte by 1) vector of log densities of Xte
%
%    (C) Copyright Hannes Nickisch, May 27th 2009.

% analyze the number of clusters in the training data
[yu,ignore,id] = unique(y); % yu(id) == y
Nc = numel(yu); N = length(y);
for i=1:Nc, h(i)=sum(id==i)/N; end % compute a normalized histogram
if min(h)<1e-2 || Nc>N/4
    fprintf('    => switched to KDE due to heavy imbalance\n')
    [lpte,s] = kde(X',Xte'); lp = kde(X',X');
    return
end

% break the data set into Nc classes
for i=1:Nc, Xc{i} = X(:,id==i); end

% density of training cases depends on their class membership
lp = -Inf(N,1);
for i=1:Nc % iterate over the classes
    [lp(id==i),s(:,i)] = kde(Xc{i}',Xc{i}');
end

% density of test cases comes from a mixture
lpte = -Inf(size(Xte,2),Nc);
for i=1:Nc % iterate over the classes
    a = h(i); % weighting according to training classes
    % a = 1/Nc; % equal weighting
    lpte(:,i) = kde(Xc{i}',Xte')+log(a); % predict under all classes
end
lpte = logsum2exp(lpte); % combine


%% computes y = log(sum(exp(x),2)) in a numerically safe way by subtracting the
%  row maximum to avoid cancelation after taking the exp
%  the sum is done along the rows
function [y,x] = logsum2exp(logx)
    N = size(logx,2);
    max_logx = max(logx,[],2);
    % we have all values in the log domain, and want to calculate a sum
    x = exp(logx-max_logx*ones(1,N));
    y = log(sum(x,2)) + max_logx;