clear all;
load  id_lfw;
load lbp_lfw;

save_dir = './learned/';

people = get_lfw_15(id_lfw);

for i = 1: size(people,1)
    person_id = people(i,1);
    start = people(i,2);
    offset = people(i,3);
    learning_identity_space(person_id, start, offset, lbp_lfw, save_dir)
end
